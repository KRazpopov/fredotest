﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxMultiplicationThree
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 4, 5, 6, 7, 55, 6, 7, 8, 5, 3, 8, 7, 3, 5, 78, 6 };
            int temp = arr.Max();
            Dictionary<int, int> dict = new Dictionary<int, int>();
            for (int i = 1; i < arr.Length - 1; i++)
            {
                if (arr[i - 1] * arr[i] * arr[i + 1] > temp)
                {
                    dict.Add(i, arr[i - 1] * arr[i] * arr[i + 1]);

                }
            }

            foreach (var a in dict)
            {
                Console.WriteLine(a.Key+"   "+a.Value);
            };
            Console.WriteLine(dict.Values.Max());
            Console.Read();
            }
    }
}

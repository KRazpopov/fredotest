﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recursion1
{
    class Program
    {
        static void Main(string[] args)
        {
             int fact(int n)
            {
                if (n <= 1)
                {
                    return n;

                }
                else
                {
                    return n * fact(n - 1);

                }


            };

            Console.WriteLine(fact(3));
            Console.Read();

        }
    }
}
